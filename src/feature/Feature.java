package feature;

import java.util.HashMap;
import java.util.Vector;

/*
 * (p_i + sum {p_{feature_i} }) * (q_i + sum {q_feature_i})'
 */
public class Feature {
    static HashMap<Long, Feature> mp = new HashMap<>();

    long id; // 可以是一个userID或者是一个itemID
    Vector<Integer> feathers; // 对应的一个user或者item的所有feature编号

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the feathers
     */
    public Vector<Integer> getFeathers() {
        return feathers;
    }

    /*
     * 不存在就返回null
     */
    public static Feature getFeatherById(long id) {
        return mp.get(id);
    }

    public Feature(long id, Vector<Integer> feathers) {
        this.id = id;
        this.feathers = feathers;
        mp.put(id, this);
    }

}
