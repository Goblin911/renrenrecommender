package feature;

import java.io.PrintStream;
import java.util.Scanner;
import java.util.Vector;

import com.renren.api.service.Sex;

import infomation.*;

public class FeatureInit {

	/*
     * 将原始数据变成feature的形式放到out里面去
     * 届时out将是一个文件的输出流
     */
	
	/*
	 Feature List: 1 FEMALE 2 MALE 3 blog 4 photo*/
    public void outputFeature(UserInfo[] users, BlogInfo[] blogs, PhotoInfo[] photos, PrintStream out) {
    	for (int i = 0; i < users.length; ++i) {
    		out.print(((Long) (users[i].user.getId())).toString());
    		if (users[i].user.getBasicInformation() != null && users[i].user.getBasicInformation().getSex() != null) {
	    		if (users[i].user.getBasicInformation().getSex() == Sex.FEMALE) {
	    			out.print(" 1");
	    		} else {
	    			out.print(" 2");
	    		}
    		}
    		out.println("");
    	}
    	for (int i = 0; i < blogs.length; ++i) {
    		out.println(((Long) (blogs[i].blog.getId())).toString() + " 3" );
    	}
    	for (int i = 0; i < photos.length; ++i) {
    		out.println(((Long) (photos[i].photo.getId())).toString() + " 4");
    	}
    }

    /*
     * 给一个Scanner，就是outputFeather输出的文件，得到
     * 一个Feature
     */
    public Feature[] loadFeature(Scanner in) {
    	Vector<Feature> res = new Vector<Feature>();
    	String nowData;
    	String[] partsOfData;
    	long id;
    	Vector<Integer> feathers;
    	while (in.hasNextLine()) {
    		nowData = in.nextLine();
    		partsOfData = nowData.split(" ");
    		id = Long.parseLong(partsOfData[0]);
    		feathers = new Vector<Integer>();
    		for (int i = 1; i < partsOfData.length; ++i) feathers.add(Integer.parseInt(partsOfData[i]));
    		res.add(new Feature(id, feathers));
    	}
    	Feature[] res2 = new Feature[res.size()];
    	for (int i = 0; i < res.size(); ++i) res2[i] = res.get(i);
    	return res2;
    }


}
