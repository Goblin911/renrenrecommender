package infomation;

import java.util.Vector;

import com.renren.api.service.*;

import data.RenrenClient;

public class PhotoInfo {
	public long ownerId;
	public Photo photo;
	public Comment[] comments;
	public LikeInfo likeInfo;

	void getCommentsFromClient(RenrenClient client) {
		int t = photo.getCommentCount();
		Vector<Comment> v = new Vector<>();
		for (int i = 0; i * 100 <= t; i++) {
			Comment[] tmp = client.listComment(true, 100, i + 1,
					CommentType.PHOTO, ownerId, photo.getId());
			for (Comment c : tmp)
				v.add(c);
		}
		comments = new Comment[v.size()];
		comments = v.toArray(comments);
	}

	void getLikeInfo(RenrenClient client) {
		likeInfo = client.getLikeUgcInfo(50, true, LikeUGCType.TYPE_PHOTO,
				photo.getId());
	}

	public PhotoInfo(RenrenClient client, long ownerId, Photo photo) {
		this.photo = photo;
		this.ownerId = ownerId;
		getCommentsFromClient(client);
		getLikeInfo(client);
	}
}
