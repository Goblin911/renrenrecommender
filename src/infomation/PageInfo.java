package infomation;


import data.RenrenClient;

public class PageInfo extends Info {

	final static int friendLimit = 5000;
	final static int commentLimit = 20;
	final static int numLimit = 100;
	public com.renren.api.service.PageInfo pageInfo;
	
	public PageInfo(RenrenClient client,
			com.renren.api.service.PageInfo pageInfo) {
		super(client, pageInfo.getId());
		this.pageInfo = pageInfo;
	}
}
