package infomation;

import java.util.Vector;

import com.renren.api.service.Blog;
import com.renren.api.service.Comment;
import com.renren.api.service.CommentType;
import com.renren.api.service.LikeInfo;
import com.renren.api.service.LikeUGCType;

import data.RenrenClient;

public class BlogInfo {
	public long ownerId;
	public Blog blog;
	public Comment[] comments;
	public LikeInfo likeInfo;

	void getCommentsFromClient(RenrenClient client) {
		int t = blog.getCommentCount();
		Vector<Comment> v = new Vector<>();
		for (int i = 0; i * 100 <= t; i++) {
			Comment[] tmp = client.listComment(true, 100, i + 1,
					CommentType.BLOG, ownerId, blog.getId());
			if (tmp == null || tmp.length == 0)
				break;
			for (Comment c : tmp)
				v.add(c);
		}
		comments = new Comment[v.size()];
		comments = v.toArray(comments);
	}

	void getLikeInfo(RenrenClient client) {
		likeInfo = client.getLikeUgcInfo(50, true, LikeUGCType.TYPE_BLOG,
				blog.getId());
	}

	public BlogInfo(RenrenClient client, long ownerId, Blog blog) {
		this.blog = blog;
		this.ownerId = ownerId;
		getCommentsFromClient(client);
		getLikeInfo(client);
	}
}
