package infomation;

import java.util.*;

import data.RenrenClient;

public class FriendsInfo {
	long id;
	Long[] friendList;

	void getFriendsFromClient(RenrenClient client, long id) {
		int pageSize = 500;
		Vector<Long> v = new Vector<>();
		for (int i = 1;; i++) {
			Integer[] tmp = client.listFriend(id, pageSize, i);
			if (tmp == null || tmp.length == 0)
				break;
			for (int j = 0; j < tmp.length; j++) {
				v.add((long) tmp[j]);
			}
		}
		friendList = new Long[v.size()];
		friendList = v.toArray(friendList);
	}

	public FriendsInfo(RenrenClient client, long id) {
		this.id = id;
		getFriendsFromClient(client, id);
	}

}
