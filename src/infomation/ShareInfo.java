package infomation;

import com.renren.api.service.*;

import data.RenrenClient;

public class ShareInfo {
	public long ownerId;
	public Share share;
	public Comment[] comments;
	public LikeInfo likeInfo;
	void getCommentsFromClient(RenrenClient client) {
		int t = Integer.parseInt(share.getCommentCount());
		comments = new Comment[t];
		int k = 0;
		for (int i = 0; i * 100 <= t; i++) {
			Comment[] tmp = client.listComment(true, 100, i + 1,
					CommentType.SHARE, ownerId, share.getId());
			if (tmp == null)
				continue;
			for (Comment c : tmp)
				comments[k++] = c;
		}
	}

	void getLikeInfo(RenrenClient client) {
		likeInfo = client.getLikeUgcInfo(50, true, LikeUGCType.TYPE_BLOG,
				share.getId());
	}

	public ShareInfo(RenrenClient client, long ownerId, Share share) {
		this.share = share;
		this.ownerId = ownerId;
		getCommentsFromClient(client);
		getLikeInfo(client);
	}
}
