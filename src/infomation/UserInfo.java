package infomation;

import com.renren.api.service.*;
import data.RenrenClient;

public class UserInfo extends Info {
    public User user;

    void getUserFromClient(RenrenClient client) {
        user = client.getUser(id);
    }

    public UserInfo(RenrenClient client, User user) {
        super(client, user.getId());
        this.user = user;
    }

    public UserInfo(RenrenClient client, long id) {
        super(client, id);
        user = client.getUser(id);
    }

}
