package infomation;

import com.renren.api.service.*;
import data.RenrenClient;
/*
 * UserInfo 和 PageInfo的基类
 */
public class Info {
	public long id;
	public Profile profile;

	Info(RenrenClient client, long id) {
		this.id = id;
		this.profile = client.getProfile(id);
	}
}
