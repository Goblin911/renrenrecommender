package data;

import infomation.FriendsInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import com.google.gson.Gson;

public class FriendsLoader {
	File file;
	BufferedReader in;

	FriendsInfo[] load() {
		FriendsInfo[] info;
		Vector<FriendsInfo> v = new Vector<>();
		while (true) {
			try {
				String s = in.readLine();
				if (s == null)
					break;
				Gson gson = new Gson();
				FriendsInfo tmp = gson.fromJson(s, FriendsInfo.class);
				v.add(tmp);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		info = new FriendsInfo[v.size()];
		info = v.toArray(info);
		return info;
	}

	public FriendsLoader(File file) throws FileNotFoundException {
		this.file = file;
		in = new BufferedReader(new FileReader(file));
	}
}
