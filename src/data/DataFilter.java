package data;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.renren.api.service.Comment;
import com.renren.api.service.LikeInfo;
import com.renren.api.service.User;

import infomation.*;

/*
 * 筛选资源
 */
public class DataFilter {

    final static int userLimit = 5;
    final static int resourceLimit = 50;
    BlogInfo[] blogs;
    PhotoInfo[] photos;
    ShareInfo[] shares;
    UserInfo[] users;

    private void addLikeInfo(HashMap<Long, Integer> mp, LikeInfo likeInfo) {
        for (User i : likeInfo.getLikeUsers()) {
            int num = 1;
            if (mp.containsKey(i.getId()))
                num = 1 + mp.get(i.getId());
            mp.put(i.getId(), num);
        }
    }

    private void addCommentsInfo(HashMap<Long, Integer> mp, Comment[] comments) {
        for (Comment i : comments) {
            int num = 1;
            if (mp.containsKey(i.getAuthorId()))
                num = 1 + mp.get(i.getAuthorId());
            mp.put(i.getAuthorId(), num);
        }
    }

    public void run(PrintStream blogsOut, PrintStream photosOut,
            PrintStream sharesOut, PrintStream usersOut) {
        HashMap<Long, UserInfo> mp = new HashMap<>();
        HashMap<Long, Integer> count = new HashMap<>();
        int blogCount = 0;
        int photoCount = 0;
        int shareCount = 0;
        int userCount = 0;
        for (UserInfo i : users) {
            mp.put(i.id, i);
        }
        for (BlogInfo i : blogs) {
            if (i.comments.length + i.likeInfo.getLikeUsers().size() >= resourceLimit) {
                Gson g = new Gson();
                blogsOut.println(g.toJson(i));
                addLikeInfo(count, i.likeInfo);
                addCommentsInfo(count, i.comments);
                blogCount++;
            }
        }
        for (PhotoInfo i : photos) {
            if (i.comments.length + i.likeInfo.getLikeUsers().size() >= resourceLimit) {
                Gson g = new Gson();
                photosOut.println(g.toJson(i));
                addLikeInfo(count, i.likeInfo);
                addCommentsInfo(count, i.comments);
                photoCount++;
            }
        }
        for (ShareInfo i : shares) {
            if (i.comments.length + i.likeInfo.getLikeUsers().size() >= resourceLimit) {
                Gson g = new Gson();
                sharesOut.println(g.toJson(i));
                addLikeInfo(count, i.likeInfo);
                addCommentsInfo(count, i.comments);
                shareCount++;
            }
        }
        for (Map.Entry<Long, Integer> i : count.entrySet()) {
            if (i.getValue() >= userLimit) {
                UserInfo info = mp.get(i.getKey());
                if (info == null || info.profile == null)
                    continue;
                Gson g = new Gson();
                usersOut.println(g.toJson(info));
                userCount++;
            }
        }
        System.out.println("blog's count : " + blogCount);
        System.out.println("share's count : " + shareCount);
        System.out.println("photo's count : " + photoCount);
        System.out.println("user's count : " + userCount);
    }

    public DataFilter(UserInfo[] users, BlogInfo[] blogs, PhotoInfo[] photos,
            ShareInfo[] shares) {
        this.users = users;
        this.blogs = blogs;
        this.photos = photos;
        this.shares = shares;
    }

}
