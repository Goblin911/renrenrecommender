package data;

import java.io.PrintStream;
import java.util.*;

import com.renren.api.service.*;

import infomation.*;

public class DataSpliter {
    BlogInfo[] blogs;
    ShareInfo[] shares;
    PhotoInfo[] photos;

    public DataSpliter(BlogInfo[] blogs, ShareInfo[] shares, PhotoInfo[] photos) {
        this.blogs = blogs;
        this.shares = shares;
        this.photos = photos;
    }

    public HashMap<Long, Object> getItemIdMap() {
        HashMap<Long, Object> mp = new HashMap<>();
        for (BlogInfo blog : blogs) {
            mp.put(blog.blog.getId(), blog.blog);
        }
        for (ShareInfo share : shares) {
            mp.put(share.share.getId(), share.share);
        }
        for (PhotoInfo photo : photos) {
            mp.put(photo.photo.getId(), photo.photo);
        }
        return mp;
    }

    public DataNode[] getDataNode() {
        Vector<DataNode> v = new Vector<DataNode>();
        for (BlogInfo blog : blogs) {
            long itemId = blog.blog.getId();
            for (Comment c : blog.comments) {
                long userId = c.getAuthorId();
                DataNode node = new DataNode(userId, itemId);
                v.add(node);
            }
            for (User u : blog.likeInfo.getLikeUsers()) {
                long userId = u.getId();
                DataNode node = new DataNode(userId, itemId);
                v.add(node);
            }
        }
        for (ShareInfo share : shares) {
            long itemId = share.share.getId();
            for (Comment c : share.comments) {
                long userId = c.getAuthorId();
                DataNode node = new DataNode(userId, itemId);
                v.add(node);
            }
            for (User u : share.likeInfo.getLikeUsers()) {
                long userId = u.getId();
                DataNode node = new DataNode(userId, itemId);
                v.add(node);
            }
        }
        for (PhotoInfo photo : photos) {
            long itemId = photo.photo.getId();
            for (Comment c : photo.comments) {
                long userId = c.getAuthorId();
                DataNode node = new DataNode(userId, itemId);
                v.add(node);
            }
            for (User u : photo.likeInfo.getLikeUsers()) {
                long userId = u.getId();
                DataNode node = new DataNode(userId, itemId);
                v.add(node);
            }
        }
        DataNode[] res = new DataNode[v.size()];
        res = v.toArray(res);
        return res;
    }

    /*
     * 将数据分成6:2:2
     */
    public void split(PrintStream train, PrintStream val, PrintStream test) {
        DataNode[] nodes = getDataNode();
        Vector<DataNode> v = new Vector<>();
        for (DataNode node : nodes)
            v.add(node);
        Collections.shuffle(v);
        nodes = v.toArray(nodes);
        int a = nodes.length * 6 / 10;
        int b = nodes.length * 8 / 10;
        for (int i = 0; i < nodes.length; i++) {
            if (i <= a)
                train.println(nodes[i].toString());
            else if (i <= b)
                val.println(nodes[i].toString());
            else
                test.println(nodes[i].toString());
        }
    }

}
