package data;

import infomation.BlogInfo;
import infomation.PhotoInfo;
import infomation.ShareInfo;

import java.io.*;
import java.util.*;

import com.renren.api.service.*;

public class InfoGetter {

	public static RenrenClient client = new RenrenClient();
	public infomation.PageInfo[] pages;
	public infomation.BlogInfo[] blogs;
	public infomation.ShareInfo[] shares;
	public infomation.PhotoInfo[] photos;
	public infomation.FriendsInfo[] friends;
	public infomation.UserInfo[] users;
	void getPageInfo() {
		try {
			RenrenClient client = new RenrenClient();
			PrintStream out = new PrintStream(new File("pageInfo.txt"));
			PageInfoGetter getter = new PageInfoGetter(client, 1, 200, out);
			getter.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void loadPageInfo() {
		PageInfoLoader loader = null;
		try {
			loader = new PageInfoLoader(new File("pageInfo.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		pages = loader.load();
		System.out.println("共计得到主页数 " + pages.length);
	}

	void getBlogInfo() {
		try {
			PrintStream out = new PrintStream(new File("blogInfo.txt"));
			for (infomation.PageInfo p : pages) {
				BlogGetter getter = new BlogGetter(client, p.id, out);
				getter.run();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	void loadBlogInfo(String file) {
		BlogLoader loader = null;
		try {
			loader = new BlogLoader(new File(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		blogs = loader.load();
		System.out.println("共计得到blog数 " + blogs.length);
	}

	void getShareInfo() {
		try {
			PrintStream out = new PrintStream(new File("shareInfo.txt"));
			for (infomation.PageInfo p : pages) {
				ShareGetter getter = new ShareGetter(client, p.id, out);
				getter.run();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	void loadShareInfo(String file) {
		ShareLoader loader = null;
		try {
			loader = new ShareLoader(new File(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		shares = loader.load();
		System.out.println("共计得到share数 " + shares.length);
	}

	void getPhotoInfo() {
		try {
			PrintStream out = new PrintStream(new File("photoInfo.txt"));
			for (infomation.PageInfo p : pages) {
				PhotoGetter getter = new PhotoGetter(client, p.id, out);
				getter.run();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	void loadPhotoInfo(String file) {
		PhotoLoader loader = null;
		try {
			loader = new PhotoLoader(new File(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		photos = loader.load();
		System.out.println("共计得到photo数 " + photos.length);
	}
	
	void getFriendsInfo() {
		try {
			PrintStream out = new PrintStream(new File("friendsInfo.txt"));
			FriendsGetter getter = new FriendsGetter(client, out, pages, blogs, shares, photos);
			getter.run();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	void loadFriendsInfo() {
		FriendsLoader loader = null;
		try {
			loader = new FriendsLoader(new File("friendsInfo.txt"));
			friends = loader.load();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("共计得到friendsList数 " + friends.length);
	}

	void getUserInfo() {
		try {
			PrintStream out = new PrintStream(new File("userInfo.txt"));
			UserGetter getter = new UserGetter(client, out, pages, blogs, shares, photos);
			getter.run();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	void loadUserInfo(String file) {
		UserLoader loader = null;
		try {
			loader = new UserLoader(new File(file));
			users = loader.load();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("共计得到usersList数 " + users.length);
	}
	
	
	// 统计一下每个用户做了几件事情
	void calc() {
		HashMap<Long, Integer> h = new HashMap<>();
		int sums = 0;
		for (BlogInfo blog : blogs) {
			for (Comment c : blog.comments)
				if (c != null) {
					sums++;
					int t = 1;
					if (h.containsKey(c.getAuthorId())) {
						t += h.get(c.getAuthorId());
					}
					h.put(c.getAuthorId(), t);
				}
			for (User u : blog.likeInfo.getLikeUsers()) {
				sums++;
				int t = 1;
				if (h.containsKey(u.getId())) {
					t += h.get(u.getId());
				}
				h.put(u.getId(), t);
			}
		}
		for (ShareInfo share : shares) {
			for (Comment c : share.comments)
				if (c != null) {
					sums++;
					int t = 1;
					if (h.containsKey(c.getAuthorId())) {
						t += h.get(c.getAuthorId());
					}
					h.put(c.getAuthorId(), t);
				}
			for (User u : share.likeInfo.getLikeUsers()) {
				sums++;
				int t = 1;
				if (h.containsKey(u.getId())) {
					t += h.get(u.getId());
				}
				h.put(u.getId(), t);
			}
		}
		for (PhotoInfo photo : photos) {
			for (Comment c : photo.comments)
				if (c != null) {
					sums++;
					int t = 1;
					if (h.containsKey(c.getAuthorId())) {
						t += h.get(c.getAuthorId());
					}
					h.put(c.getAuthorId(), t);
				}
			for (User u : photo.likeInfo.getLikeUsers()) {
				sums++;
				int t = 1;
				if (h.containsKey(u.getId())) {
					t += h.get(u.getId());
				}
				h.put(u.getId(), t);
			}
		}
		System.out.println("评论和赞的总数  " + sums);
		int[] sum = new int[500];
		for (Map.Entry<Long, Integer> i : h.entrySet()) {
			sum[i.getValue()]++;
		}

		for (int i = 0; i < sum.length; i++)
			if (sum[i] > 0)
				System.out.println(i + " : " + sum[i]);
	}
	
	public void loadAll() {
		//loadPageInfo();
		loadBlogInfo("data/blogs.dat");
		loadShareInfo("data/shares.dat");
		loadPhotoInfo("data/photos.dat");
        loadUserInfo("data/users.dat");
	}

}
