package data;

import java.io.PrintStream;
import java.util.Vector;

import tools.Tools;
import infomation.*;

public class PageInfoGetter implements Runnable {
	final static int pageSize = 500;
	RenrenClient client;
	int start, end;
	PrintStream out;

	public PageInfoGetter(RenrenClient client, int start, int end,
			PrintStream out) {
		this.client = client;
		this.start = start;
		this.end = end;
		this.out = out;
	}

	@Override
	public void run() {
		System.out.println("start");
		Vector<PageInfo> infos = new Vector<>();
		for (int i = start; i < end; i++) {
			com.renren.api.service.PageInfo[] pages = client.listPage(pageSize,
					i);
			if (pages == null || pages.length == 0)
				break;
			for (com.renren.api.service.PageInfo p : pages) {
				if (p.getFansCount() < 10000)
					continue;
				PageInfo info = new PageInfo(client, p);
				String line = Tools.toString(info);
				out.println(line);
				out.flush();
				Tools.show(info);
				infos.add(info);
			}
		}
	}

}
