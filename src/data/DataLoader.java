package data;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class DataLoader {
    BufferedReader in;

    public DataNode[] getDataNodes() {
        Vector<DataNode> v = new Vector<>();
        while (true) {
            String s = null;
            try {
                s = in.readLine();
            } catch (IOException e) {
                break;
            }
            if (s == null)
                break;
            DataNode node = new DataNode(s);
            v.add(node);
        }
        DataNode[] res = new DataNode[v.size()];
        res = v.toArray(res);
        return res;
    }


    public DataLoader(File inputFile) throws FileNotFoundException{
        in = new BufferedReader(new FileReader(inputFile));
    }
}
