package data;

import infomation.BlogInfo;
import infomation.PageInfo;
import infomation.PhotoInfo;
import infomation.ShareInfo;
import infomation.UserInfo;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;

import tools.Tools;

import com.renren.api.service.Comment;
import com.renren.api.service.User;

public class UserGetter implements Runnable {
	final static int limit = 4; // 至少做过4件事情
	PrintStream out;
	PageInfo[] pages;
	BlogInfo[] blogs;
	ShareInfo[] shares;
	PhotoInfo[] photos;
	HashMap<Long, Integer> userToComments;
	RenrenClient client;
	void calcUserToComments() {
		userToComments = new HashMap<>();
		for (BlogInfo blog : blogs) {
			for (Comment c : blog.comments)
				if (c != null) {
					int t = 1;
					if (userToComments.containsKey(c.getAuthorId())) {
						t += userToComments.get(c.getAuthorId());
					}
					userToComments.put(c.getAuthorId(), t);
				}
			for (User u : blog.likeInfo.getLikeUsers()) {
				int t = 1;
				if (userToComments.containsKey(u.getId())) {
					t += userToComments.get(u.getId());
				}
				userToComments.put(u.getId(), t);
			}
		}
		for (ShareInfo share : shares) {
			for (Comment c : share.comments)
				if (c != null) {
					int t = 1;
					if (userToComments.containsKey(c.getAuthorId())) {
						t += userToComments.get(c.getAuthorId());
					}
					userToComments.put(c.getAuthorId(), t);
				}
			for (User u : share.likeInfo.getLikeUsers()) {
				int t = 1;
				if (userToComments.containsKey(u.getId())) {
					t += userToComments.get(u.getId());
				}
				userToComments.put(u.getId(), t);
			}
		}
		for (PhotoInfo photo : photos) {
			for (Comment c : photo.comments)
				if (c != null) {
					int t = 1;
					if (userToComments.containsKey(c.getAuthorId())) {
						t += userToComments.get(c.getAuthorId());
					}
					userToComments.put(c.getAuthorId(), t);
				}
			for (User u : photo.likeInfo.getLikeUsers()) {
				int t = 1;
				if (userToComments.containsKey(u.getId())) {
					t += userToComments.get(u.getId());
				}
				userToComments.put(u.getId(), t);
			}
		}
	}

	public UserGetter(RenrenClient client, PrintStream out, PageInfo[] pages, BlogInfo[] blogs,
			ShareInfo[] shares, PhotoInfo[] photos) {
		this.client = client;
		this.out = out;
		this.pages = pages;
		this.blogs = blogs;
		this.shares = shares;
		this.photos = photos;
	}

	@Override
	public void run() {
		calcUserToComments();
		HashSet<Integer> flag = new HashSet<>();
		for (BlogInfo blog : blogs) {
			for (Comment c : blog.comments)
				if (c != null && userToComments.get(c.getAuthorId()) >= limit) {
					if (!flag.contains(c.getAuthorId())) {
						UserInfo info = new UserInfo(client, c.getAuthorId());
						String line = Tools.toString(info);
						out.println(line);
						out.flush();
						Tools.show(info);
					}
				}
			if (blog.likeInfo == null)
				continue;
			for (User u : blog.likeInfo.getLikeUsers()) {
				if (userToComments.get(u.getId()) >= limit) {
					if (!flag.contains(u.getId())) {
						UserInfo info = new UserInfo(client, u);
						String line = Tools.toString(info);
						out.println(line);
						out.flush();
						Tools.show(info);
					}
				}
			}
		}
		for (ShareInfo share : shares) {
			for (Comment c : share.comments)
				if (c != null && userToComments.get(c.getAuthorId()) >= limit) {
					if (!flag.contains(c.getAuthorId())) {
						UserInfo info = new UserInfo(client, c.getAuthorId());
						String line = Tools.toString(info);
						out.println(line);
						out.flush();
						Tools.show(info);
					}
				}
			if (share.likeInfo == null)
				continue;
			for (User u : share.likeInfo.getLikeUsers()) {
				if (userToComments.get(u.getId()) >= limit) {
					if (!flag.contains(u.getId())) {
						UserInfo info = new UserInfo(client, u);
						String line = Tools.toString(info);
						out.println(line);
						out.flush();
						Tools.show(info);
					}
				}
			}
		}
		for (PhotoInfo photo : photos) {
			for (Comment c : photo.comments)
				if (c != null && userToComments.get(c.getAuthorId()) >= limit) {
					if (!flag.contains(c.getAuthorId())) {
						UserInfo info = new UserInfo(client, c.getAuthorId());
						String line = Tools.toString(info);
						out.println(line);
						out.flush();
						Tools.show(info);
					}
				}
			if (photo.likeInfo == null)
				continue;
			for (User u : photo.likeInfo.getLikeUsers()) {
				if (userToComments.get(u.getId()) >= limit) {
					if (!flag.contains(u.getId())) {
						UserInfo info = new UserInfo(client, u);
						String line = Tools.toString(info);
						out.println(line);
						out.flush();
						Tools.show(info);
					}
				}
			}
		}
	}
}
