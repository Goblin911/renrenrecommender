package data;

import infomation.FriendsInfo;
import infomation.UserInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import com.google.gson.Gson;

public class UserLoader {
	File file;
	BufferedReader in;

	UserInfo[] load() {
		UserInfo[] info;
		Vector<UserInfo> v = new Vector<>();
		while (true) {
			try {
				String s = in.readLine();
				if (s == null)
					break;
				Gson gson = new Gson();
				UserInfo tmp = gson.fromJson(s, UserInfo.class);
				v.add(tmp);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		info = new UserInfo[v.size()];
		info = v.toArray(info);
		return info;
	}

	public UserLoader(File file) throws FileNotFoundException {
		this.file = file;
		in = new BufferedReader(new FileReader(file));
	}
}
