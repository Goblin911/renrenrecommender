package data;

import infomation.PhotoInfo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import com.google.gson.Gson;

public class PhotoLoader {
	File file;
	BufferedReader in;
	PhotoInfo[] load() {
		PhotoInfo[] info;
		Vector<PhotoInfo> v = new Vector<>();
		while (true) {
			try {
				String s = in.readLine();
				if (s == null)
					break;
				Gson gson = new Gson();
				PhotoInfo tmp = gson.fromJson(s, PhotoInfo.class);
				v.add(tmp);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		info = new PhotoInfo[v.size()];
		info = v.toArray(info);
		return info;
	}
	
	public PhotoLoader(File file) throws FileNotFoundException {
		this.file = file;
		in = new BufferedReader(new FileReader(file));
	}

}
