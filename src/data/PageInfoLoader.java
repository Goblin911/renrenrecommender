package data;

import infomation.PageInfo;

import java.io.*;
import java.util.Vector;

import com.google.gson.Gson;

public class PageInfoLoader {
	File file;
	BufferedReader in;
	PageInfo[] load() {
		PageInfo[] info;
		Vector<PageInfo> v = new Vector<>();
		while (true) {
			try {
				String s = in.readLine();
				if (s == null)
					break;
				Gson gson = new Gson();
				PageInfo tmp = gson.fromJson(s, PageInfo.class);
				v.add(tmp);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		info = new PageInfo[v.size()];
		info = v.toArray(info);
		return info;
	}
	
	public PageInfoLoader(File file) throws FileNotFoundException {
		this.file = file;
		in = new BufferedReader(new FileReader(file));
	}
}
