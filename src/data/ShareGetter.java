package data;

import infomation.ShareInfo;
import java.io.PrintStream;
import java.util.Vector;
import tools.Tools;
import com.renren.api.service.Share;

public class ShareGetter implements Runnable{
	final static int commentLimit = 15;
	final static int numLimit = 50;
	RenrenClient client;
	PrintStream out;
	long id;
	
	public ShareGetter(RenrenClient client, long id, PrintStream out) {
		this.client = client;
		this.id = id;
		this.out = out;
	}
	@Override
	public void run() {
		Share[] tmp = null;
		try {
			tmp = client.listShare(id, 100, 1);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		if (tmp == null)
			return;
		Vector<Share> t = new Vector<>();
		for (int i = 0; i < tmp.length; i++) {
			if (Integer.parseInt(tmp[i].getCommentCount()) < commentLimit)
				continue;
			if (t.size() > numLimit)
				break;
			if (!tmp[i].getShareTime().substring(0, 4).equals("2014"))
				continue;
			ShareInfo info = new ShareInfo(client, id, tmp[i]);
			String line = Tools.toString(info);
			out.println(line);
			out.flush();
			Tools.show(info);
		}
		
	}

}
