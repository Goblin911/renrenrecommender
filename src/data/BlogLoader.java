package data;

import infomation.BlogInfo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import com.google.gson.Gson;

public class BlogLoader {
	File file;
	BufferedReader in;
	BlogInfo[] load() {
		BlogInfo[] info;
		Vector<BlogInfo> v = new Vector<>();
		while (true) {
			try {
				String s = in.readLine();
				if (s == null)
					break;
				Gson gson = new Gson();
				BlogInfo tmp = gson.fromJson(s, BlogInfo.class);
				v.add(tmp);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		info = new BlogInfo[v.size()];
		info = v.toArray(info);
		return info;
	}
	
	public BlogLoader(File file) throws FileNotFoundException {
		this.file = file;
		in = new BufferedReader(new FileReader(file));
	}
}
