package data;

import infomation.PhotoInfo;

import java.io.PrintStream;
import tools.Tools;
import com.renren.api.service.*;

public class PhotoGetter implements Runnable {
	final static int commentLimit = 10;
	final static int numLimit = 30;
	RenrenClient client;
	PrintStream out;
	long id;
	
	public PhotoGetter(RenrenClient client, long id, PrintStream out) {
		this.client = client;
		this.id = id;
		this.out = out;
	}
	
	@Override
	public void run() {
		Album[] albums = client.listAlbum(id, 100, 1);
		for (Album album : albums) {
			int count = 0;
			Photo[] photos = client.listPhoto(album.getId(), id, 100, 1);
			for (Photo p : photos) {
				if (p.getCommentCount() < commentLimit)
					continue;
				if (count > numLimit)
					break;
				if (!p.getCreateTime().subSequence(0, 4).equals("2014"))
					continue;
				count++;
				PhotoInfo info = new PhotoInfo(client, id, p);
				String line = Tools.toString(info);
				out.println(line);
				out.flush();
				Tools.show(info);
			}
		}
	}
}
