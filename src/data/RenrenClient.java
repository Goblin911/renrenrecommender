package data;

import com.renren.api.*;
import com.renren.api.service.*;

public class RenrenClient {
	final static String API_KEY = "2743f000a4764021a4123e67efe908d8";
	final static String SECRET_KEY = "cce73b9d5ad24110ae0ccfa5f747fdfd";
	final static String[] TOKEN = {
			"270435|6.f7aa1bda67650debd7b441a936a14b74.2592000.1409648400-832201190",
			"270435|6.0519bd95090d49d8b4ebbe831b939dac.2592000.1409749200-832940551",
			"270435|6.9d08483311bcbee01fb8986e4961a61e.2592000.1409749200-832931403",
			"270435|6.c0063a6342aa5e33c085c12763451451.2592000.1409749200-832934447",
			"270435|6.87b3ad5857c640b8df30db12cd1d3c1c.2592000.1409749200-832935449",
			"270435|6.368c6b3a35c8a86fe131566aa09a9c8e.2592000.1409770800-833010313",
			"270435|6.65bc8a4534a56f50a6207b070d4fb0ae.2592000.1409770800-833011017",
			"270435|6.494fd09c79e70e57004599c4e6232a87.2592000.1409835600-833205917",
			"270435|6.eb5c4c0ff655228df29b31c008e6461e.2592000.1409839200-833208894",
			"270435|6.c3247cdeb6b5d4ed0916d29665392de8.2592000.1409904000-833403792",
			"270435|6.3b1c7865d304f383e04ac91b44678df2.2592000.1409986800-833703749",
			"270435|6.9c5ce390d61ab2544256e2230323fc51.2592000.1409994000-833739461",
			"270435|6.e44fa86a055f9988e5378277963f23ad.2592000.1410001200-833761381",
			"270435|6.527f9e9dce13d5b7b81d4e4e632e59f6.2592000.1410008400-833793790",
			"270435|6.90ad3885930fe487da3b8f7c3d2a906e.2592000.1410019200-833858389" };
	RennClient[] client;
	long[] lastReq;
	final static int n = 15;
	long sleepTime = 25 * 1000; // 每个用户25s请求一次

	int getNonbusyClientNum() {
		while (true) {
			int num = -1;
			long tm = Long.MAX_VALUE;
			for (int i = 0; i < client.length; i++) {
				if (lastReq[i] + sleepTime < tm) {
					num = i;
					tm = lastReq[i] + sleepTime;
				}
			}
			if (tm < System.currentTimeMillis())
				return num;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	enum RequestType {
		getProfile, listFriend, getPage, listPage, batchUser, getUser, getLikeUgcInfo, listComment, listBlog, listAlbum, listShare, listPhoto
	}

	Object request(RequestType type, Object[] param) {
		Object res = null;
		int num = 0;
		int retry = 0;
		while (retry < 5) {
			num = getNonbusyClientNum();
			try {
				RennClient client = this.client[num];
				switch (type) {
				case listPhoto: {
					res = client.getPhotoService().listPhoto((long) param[0],
							(long) param[1], (int) param[2], (int) param[3],
							null);

					break;
				}
				case listShare: {
					res = client.getShareService().listShare((long) param[0],
							(int) param[1], (int) param[2]);
					break;
				}
				case listAlbum: {
					res = client.getAlbumService().listAlbum((long) param[0],
							(int) param[1], (int) param[2]);
					break;
				}
				case listBlog: {
					res = client.getBlogService().listBlog((long) param[0],
							(int) param[1], (int) param[2]);
					break;
				}
				case listComment: {
					res = client.getCommentService().listComment(
							(boolean) param[0], (int) param[1], (int) param[2],
							(CommentType) param[3], (long) param[4],
							(long) param[5]);
					break;
				}
				case getLikeUgcInfo: {
					res = client.getLikeService().getLikeUgcInfo(
							(int) param[0], (boolean) param[1],
							(LikeUGCType) param[2], (long) param[3]);
					break;
				}
				case getUser: {
					res = client.getUserService().getUser((long) param[0]);
					break;
				}
				case listPage: {
					res = client.getPageService().listPage((int) param[0],
							(int) param[1]);
					break;
				}
				case getProfile: {
					long id = (long) param[0];
					res = client.getProfileService().getProfile(id);
					break;
				}
				case listFriend: {
					long id = (long) param[0];
					int pageSize = (int) param[1];
					int pageNum = (int) param[2];
					res = client.getFriendService().listFriend(id, pageSize,
							pageNum);
					break;
				}
				case getPage: {
					int pageId = (int) param[0];
					res = client.getPageService().getPage(pageId);
					return res;
				}
				case batchUser: {
					Long[] userIds = (Long[]) param[0];
					client.getUserService().batchUser(userIds);
					break;
				}
				default:
					break;
				}
				break;
			} catch (Exception e) {
				e.printStackTrace();
				if (e.getMessage().equals("用户已经没有分享可以显示。"))
					break;
				lastReq[num] = System.currentTimeMillis()
						+ ((int) Math.random() * 3000);
				System.err.println("retry time : " + ++retry);
				if (e.getMessage().equals("请不要频繁调用接口，您的调用次数已超过了限制!")) {
					lastReq[num] += 5 * 60 * 1000;
					retry--;
				}
				if (e.getMessage().equals("未知异常。")) {
					if (Math.random() < 0.1) retry--;
				}
				try {
					Thread.sleep(1000 * 30);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
					System.exit(1);
				}
			}
		}
		lastReq[num] = System.currentTimeMillis()
				+ ((int) Math.random() * 3000);
		return res;
	}

	public User[] batchUser(Long[] userIds) {
		Object[] param = new Object[1];
		param[0] = userIds;
		Object res = request(RequestType.batchUser, param);
		return (User[]) res;
	}

	public PageInfo getPage(int id) {
		Object[] param = new Object[1];
		param[0] = id;
		Object res = request(RequestType.getPage, param);
		return (PageInfo) res;
	}

	public Profile getProfile(long id) {
		Object[] param = new Object[1];
		param[0] = id;
		Object res = request(RequestType.getProfile, param);
		return (Profile) res;
	}

	public Integer[] listFriend(long id, int pageSize, int pageNum) {
		Object[] param = new Object[3];
		param[0] = id;
		param[1] = pageSize;
		param[2] = pageNum;
		Object res = request(RequestType.listFriend, param);
		return (Integer[]) res;
	}

	public PageInfo[] listPage(int pageSize, int pageNum) {
		Object[] param = new Object[2];
		param[0] = pageSize;
		param[1] = pageNum;
		Object res = request(RequestType.listPage, param);
		return (PageInfo[]) res;
	}

	public User getUser(long id) {
		Object[] param = new Object[1];
		param[0] = id;
		Object res = request(RequestType.getUser, param);
		return (User) res;
	}

	public LikeInfo getLikeUgcInfo(Integer limit, Boolean withLikeUsers,
			LikeUGCType likeUGCType, Long ugcId) {
		Object[] param = new Object[4];
		param[0] = limit;
		param[1] = withLikeUsers;
		param[2] = likeUGCType;
		param[3] = ugcId;
		Object res = request(RequestType.getLikeUgcInfo, param);
		return (LikeInfo) res;

	}

	public Comment[] listComment(Boolean desc, Integer pageSize,
			Integer pageNumber, CommentType commentType, Long entryOwnerId,
			Long entryId) {
		Object[] param = new Object[6];
		param[0] = desc;
		param[1] = pageSize;
		param[2] = pageNumber;
		param[3] = commentType;
		param[4] = entryOwnerId;
		param[5] = entryId;
		Object res = request(RequestType.listComment, param);
		return (Comment[]) res;
	}

	public Blog[] listBlog(Long ownerId, Integer pageSize, Integer pageNumber) {
		Object[] param = new Object[3];
		param[0] = ownerId;
		param[1] = pageSize;
		param[2] = pageNumber;
		Object res = request(RequestType.listBlog, param);
		return (Blog[]) res;
	}

	public Album[] listAlbum(Long ownerId, Integer pageSize, Integer pageNumber) {
		Object[] param = new Object[3];
		param[0] = ownerId;
		param[1] = pageSize;
		param[2] = pageNumber;
		Object res = request(RequestType.listAlbum, param);
		return (Album[]) res;

	}

	public Share[] listShare(Long ownerId, Integer pageSize, Integer pageNumber) {
		Object[] param = new Object[3];
		param[0] = ownerId;
		param[1] = pageSize;
		param[2] = pageNumber;
		Object res = request(RequestType.listShare, param);
		return (Share[]) res;
	}

	public Photo[] listPhoto(Long albumId, Long ownerId, Integer pageSize,
			Integer pageNumber) {
		Object[] param = new Object[4];
		param[0] = albumId;
		param[1] = ownerId;
		param[2] = pageSize;
		param[3] = pageNumber;
		Object res = request(RequestType.listPhoto, param);
		return (Photo[]) res;

	}

	public RenrenClient() {
		client = new RennClient[n];
		lastReq = new long[n];
		for (int i = 0; i < n; i++) {
			client[i] = new RennClient(API_KEY, SECRET_KEY);
			try {
				client[i].authorizeWithAccessToken(TOKEN[i]);
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}

}
