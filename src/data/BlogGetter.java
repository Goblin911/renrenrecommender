package data;

import infomation.BlogInfo;

import java.io.PrintStream;
import java.util.Vector;

import tools.Tools;

import com.renren.api.service.Blog;

public class BlogGetter implements Runnable {
	final static int commentLimit = 15;
	final static int numLimit = 50;
	RenrenClient client;
	PrintStream out;
	long id;
	
	public BlogGetter(RenrenClient client, long id, PrintStream out) {
		this.client = client;
		this.id = id;
		this.out = out;
	}
	
	@Override
	public void run() {
		for (int page = 1; page <= 3; page++) {
			Blog[] tmp = client.listBlog(id, 20, page);
			if (tmp == null || tmp.length == 0)
				break;
			Vector<Blog> t = new Vector<>();
			for (int i = 0; i < tmp.length; i++) {
				if (tmp[i].getCommentCount() < commentLimit)
					continue;
				if (t.size() > numLimit)
					break;
				if (!tmp[i].getCreateTime().substring(0, 4).equals("2014"))
					continue;
				BlogInfo info = new BlogInfo(client, id, tmp[i]);
				String line = Tools.toString(info);
				out.println(line);
				out.flush();
				Tools.show(info);
			}
		}
	}

}
