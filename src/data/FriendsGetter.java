package data;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import tools.Tools;

import com.renren.api.service.Comment;
import com.renren.api.service.User;

import infomation.*;

public class FriendsGetter implements Runnable {
	final static int limit = 4; // 至少做过4件事情
	RenrenClient client;
	PrintStream out;
	PageInfo[] pages;
	BlogInfo[] blogs;
	ShareInfo[] shares;
	PhotoInfo[] photos;
	HashMap<Long, Integer> userToComments;

	void calcUserToComments() {
		userToComments = new HashMap<>();
		for (BlogInfo blog : blogs) {
			for (Comment c : blog.comments)
				if (c != null) {
					int t = 1;
					if (userToComments.containsKey(c.getAuthorId())) {
						t += userToComments.get(c.getAuthorId());
					}
					userToComments.put(c.getAuthorId(), t);
				}
			for (User u : blog.likeInfo.getLikeUsers()) {
				int t = 1;
				if (userToComments.containsKey(u.getId())) {
					t += userToComments.get(u.getId());
				}
				userToComments.put(u.getId(), t);
			}
		}
		for (ShareInfo share : shares) {
			for (Comment c : share.comments)
				if (c != null) {
					int t = 1;
					if (userToComments.containsKey(c.getAuthorId())) {
						t += userToComments.get(c.getAuthorId());
					}
					userToComments.put(c.getAuthorId(), t);
				}
			for (User u : share.likeInfo.getLikeUsers()) {
				int t = 1;
				if (userToComments.containsKey(u.getId())) {
					t += userToComments.get(u.getId());
				}
				userToComments.put(u.getId(), t);
			}
		}
		for (PhotoInfo photo : photos) {
			for (Comment c : photo.comments)
				if (c != null) {
					int t = 1;
					if (userToComments.containsKey(c.getAuthorId())) {
						t += userToComments.get(c.getAuthorId());
					}
					userToComments.put(c.getAuthorId(), t);
				}
			for (User u : photo.likeInfo.getLikeUsers()) {
				int t = 1;
				if (userToComments.containsKey(u.getId())) {
					t += userToComments.get(u.getId());
				}
				userToComments.put(u.getId(), t);
			}
		}
	}

	@Override
	public void run() {
		calcUserToComments();
		for (Map.Entry<Long, Integer> i : userToComments.entrySet())
			if (i.getValue() >= limit) {
				FriendsInfo info = new FriendsInfo(client, i.getKey());
				String line = Tools.toString(info);
				out.println(line);
				out.flush();
				Tools.show(info);
			}
	}

	public FriendsGetter(RenrenClient client, PrintStream out,
			PageInfo[] pages, BlogInfo[] blogs, ShareInfo[] shares,
			PhotoInfo[] photos) {
		this.client = client;
		this.out = out;
		this.pages = pages;
		this.blogs = blogs;
		this.shares = shares;
		this.photos = photos;
	}
}
