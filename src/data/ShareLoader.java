package data;

import infomation.ShareInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import com.google.gson.Gson;

public class ShareLoader {
	File file;
	BufferedReader in;
	ShareInfo[] load() {
		ShareInfo[] info;
		Vector<ShareInfo> v = new Vector<>();
		while (true) {
			try {
				String s = in.readLine();
				if (s == null)
					break;
				Gson gson = new Gson();
				ShareInfo tmp = gson.fromJson(s, ShareInfo.class);
				v.add(tmp);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		info = new ShareInfo[v.size()];
		info = v.toArray(info);
		return info;
	}
	
	public ShareLoader(File file) throws FileNotFoundException {
		this.file = file;
		in = new BufferedReader(new FileReader(file));
	}
}
