package data;

import com.google.gson.Gson;

public class DataNode {
    long userId;
    long itemId;

    public DataNode(long userId, long itemId) {
        this.userId = userId;
        this.itemId = itemId;
    }

    public DataNode(String s) {
        Gson g = new Gson();
        DataNode t = g.fromJson(s, DataNode.class);
        this.userId = t.userId;
        this.itemId = t.itemId;
    }

    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * @return the itemId
     */
    public long getItemId() {
        return itemId;
    }

    @Override
    public String toString() {
        Gson g = new Gson();
        String res = g.toJson(this);
        return res;
    }

}
