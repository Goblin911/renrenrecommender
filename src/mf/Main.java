package mf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Vector;

import data.*;
import feature.Feature;
import feature.FeatureInit;

public class Main {
	static PrintStream debug = System.err;
	DataNode[] trainNodes, valNodes, trainFinalnodes, testNodes;
	HashSet<Long> userIds, itemIds;
	HashMap<Long, HashSet<Long>> mp; // user To Items

    HashMap<Long, Integer> userId_mapping, itemId_mapping;

	// 两个参数
	double lamda;
	int d;
    int negative_example;

	/*
	 * 将数据分成6:2:2
	 */
	void split() {
		InfoGetter getter = new InfoGetter();
		getter.loadAll();
		DataSpliter spliter = new DataSpliter(getter.blogs, getter.shares,
				getter.photos);
		FeatureInit initializer = new FeatureInit();
		try {
			PrintStream train = new PrintStream(new File("data/train.dat"));
			PrintStream val = new PrintStream(new File("data/val.dat"));
			PrintStream test = new PrintStream(new File("data/test.dat"));
			spliter.split(train, val, test);
			PrintStream features = new PrintStream(new File("data/features.dat"));
			initializer.outputFeature(getter.users, getter.blogs,
					getter.photos, features);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/*
	 * load 三类数据
	 */
	void init() {
        userIds = new HashSet<>();
        itemIds = new HashSet<>();
		try {
			DataLoader loader;
			loader = new DataLoader(new File("data/train.dat"));
			trainNodes = loader.getDataNodes();
			loader = new DataLoader(new File("data/val.dat"));
			valNodes = loader.getDataNodes();
			trainFinalnodes = new DataNode[trainNodes.length + valNodes.length];
			int k = 0;
			for (DataNode i : trainNodes)
				trainFinalnodes[k++] = i;
			for (DataNode i : valNodes)
				trainFinalnodes[k++] = i;
			loader = new DataLoader(new File("data/test.dat"));
			testNodes = loader.getDataNodes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (DataNode i : trainNodes) {
			userIds.add(i.getUserId());
			itemIds.add(i.getItemId());
		}
		for (DataNode i : valNodes) {
			userIds.add(i.getUserId());
			itemIds.add(i.getItemId());
		}
		for (DataNode i : testNodes) {
			userIds.add(i.getUserId());
			itemIds.add(i.getItemId());
		}
	}

	class Node {
		long userId;
		long itemId;
		double value;

		Node(long userId, long itemId, double value) {
			this.userId = userId;
			this.itemId = itemId;
			this.value = value;
		}
	}

	/*
	 * 评价结果结果越大就越好
	 */
	double evaluation(GradientDescent g) {
		double ans = 0;
		for (Long userId : userIds) {
			Vector<Node> v = new Vector<>();
			for (Long itemId : itemIds) {
				double value = g.get(userId, itemId);
				Node t = new Node(userId, itemId, value);
				v.add(t);
			}
			System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
			Collections.sort(v, new Comparator<Node>() {
				@Override
				public int compare(Node a, Node b) {
					if (a.value > b.value)
						return 1;
					else if (a.value < b.value)
						return -1;
					else
						return 0;
                    
				}
			});

			HashSet<Long> items = mp.get(userId);
            if (items == null)
                continue;
			int all = items.size();
			int now = 0;
			double res = 0;
			for (int i = 0; i < v.size(); i++) {
				if (items.contains(v.get(i).itemId)) {
					now++;
					res += 1.0 / (i + 1);
					if (now == all)
						break;
				}
			}
			res /= all;
			ans += res;
		}
		ans /= userIds.size();
		return ans;
	}

	void makeMapping() { //changed
		Iterator<Long> iter = userIds.iterator();
		Long now;
		int no = 1;
        userId_mapping = new HashMap<>();
        itemId_mapping = new HashMap<>();
		do {
			now = iter.next();
			userId_mapping.put(now, (Integer) no);
			++no;
		} while(iter.hasNext());
		iter = itemIds.iterator();
		no = 1;
		do {
			now = iter.next();
			itemId_mapping.put(now, (Integer) no);
			++no;
		} while(iter.hasNext());
	}

	void train() {
        negative_example = 5;
		mp = new HashMap<>();
		for (DataNode node : valNodes) {
			HashSet<Long> item;
			if (mp.containsKey(node.getUserId()))
				item = mp.get(node.getUserId());
			else
				item = new HashSet<>();
			item.add(node.getItemId());
			mp.put(node.getUserId(), item);
		}
		FeatureInit featureInit = new FeatureInit();
		Feature[] features = null;
		try {
			features = featureInit.loadFeature(new Scanner(new File(
					"data/features.dat")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			//System.exit(1);
		}
		double ans = 0;
		this.lamda = 0.1;
		this.d = 32;
		for (double lamda = 1e-5; lamda < 1e5; lamda *= 10) {
			for (int d = 1; d <= 32; d *= 2) {
				debug.println("lamda = " + lamda + "  d = " + d);
				GradientDescent g = new GradientDescent(trainNodes, features,
						d, negative_example, lamda, userId_mapping, itemId_mapping);
				debug.println("gradient descent start!");
				g.gradientDescent();
				debug.println("gradient descent end!");
                debug.println("evaluation start!");
				double eva = evaluation(g);
                debug.println("evaluation end!");
				debug.println("eva = " + eva);
				if (ans > eva) {
					ans = eva;
					this.lamda = lamda;
					this.d = d;
				}
			}
		}
	}

	void test() {
		FeatureInit featureInit = new FeatureInit();
		Feature[] features = null;
		try {
			features = featureInit.loadFeature(new Scanner(new File(
					"data/features.dat")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
		GradientDescent g = new GradientDescent(trainFinalnodes, features, d, negative_example,
				lamda, userId_mapping, itemId_mapping); //changed
		g.gradientDescent();
		mp = new HashMap<>();
		for (DataNode node : testNodes) {
			HashSet<Long> item;
			if (mp.containsKey(node.getUserId()))
				item = mp.get(node.getUserId());
			else
				item = new HashSet<>();
			item.add(node.getItemId());
		}
		double eva = evaluation(g);
		System.out.println("final eva equals to " + eva);
	}

	void run() {
        makeMapping();
		train();
		test();
	}


    void filte() {
        InfoGetter getter = new InfoGetter();
        getter.loadAll();
        DataFilter filter = new DataFilter(getter.users, getter.blogs, getter.photos, getter.shares);
        try {
            PrintStream blogsOut = new PrintStream(new File("blogs.dat"));
            PrintStream photosOut = new PrintStream(new File("photos.dat"));
            PrintStream sharesOut = new PrintStream(new File("shares.dat"));
            PrintStream usersOut = new PrintStream(new File("users.dat"));
            filter.run(blogsOut, photosOut, sharesOut, usersOut);
        } catch (Exception e) {
            e.printStackTrace(); 
        }

    }

	public static void main(String[] args) {
		Main m = new Main();
        //m.filte();
		//m.split();
        m.init();
        System.out.println(m.testNodes.length);
		m.run();
	}
}
