package mf;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import data.DataNode;
import feature.Feature;

public class GradientDescent {
	double lamda;
	int d;
	DataNode[] nodes;
	Feature[] features;

	//double alpha;
	double[][] theta;
	double[][] x;
	double[][] feature_theta;
	double[][] feature_x;
	int user_N; // needed
	int item_N; // needed
	int feature_N;
	//int max_round;
	int negative_example;
	HashMap<Long, Integer> userId_mapping, itemId_mapping;
	HashMap<Integer, Long> userId_back_mapping, itemId_back_mapping;
	HashMap<Long, HashSet<Long>> mp_user_item;
	HashMap<Long, HashSet<Integer>> mp_id_feature;
	Random rand;
	
	final static double alpha = 0.01;
	final static int max_round = 200;

	// 鎵�湁鐨勭紪鍙锋槸浠�寮�

	public double Cost_Function_with_Regulization() {
		double res = Cost_Function(), now = 0;
		for (int i = 1; i <= user_N; ++i)
			for (int j = 1; j <= d; ++j)
				now += theta[i][j] * theta[i][j];
		now = now * lamda / 2.0;
		res += now;
		now = 0;
		for (int i = 1; i <= item_N; ++i)
			for (int j = 1; j <= d; ++j)
				now += x[i][j] * x[i][j];
		now = now * lamda / 2.0;
		res += now;
		now = 0;
		for (int i = 1; i <= feature_N; ++i)
			for (int j = 1; j <= d; ++j)
				now += feature_theta[i][j] * feature_theta[i][j] + feature_x[i][j] * feature_x[i][j];
		now = now * lamda / 2.0;
		res += now;
		return res;
	}

	public double Cost_Function() {
		int p, q;
		double res = 0.0, now;
		for (int i = 0; i < nodes.length; ++i) {
			now = get(nodes[i].getUserId(), nodes[i].getItemId()) - 1.0;
			res += now * now;
			for (int j = 1; j <= negative_example; ++j) {
				do {
					p = rand.nextInt(user_N) + 1;
				} while (userId_back_mapping.get(p) == null
						|| mp_user_item.get(userId_back_mapping.get(p)) == null);
				do {
					q = rand.nextInt(item_N) + 1;
				} while (itemId_back_mapping.get(q) == null
						|| mp_user_item.get(userId_back_mapping.get(p))
								.contains(itemId_back_mapping.get(q)));
				now = get(userId_back_mapping.get(p),
						itemId_back_mapping.get(q));
				res += now * now;
			}
		}
		res = res / 2.0;
		return res;
	}

	public double Dot_Product(double[] A, double[] B, int D) {
		double res = 0.0;
		for (int i = 1; i <= D; ++i)
			res += A[i] * B[i];
		return res;
	}

	public void Pre_Work() {
		user_N = userId_mapping.size();
		item_N = itemId_mapping.size();
		feature_N = 4;
		//max_round = 10000;
		//alpha = 1.0E-2;
		theta = new double[user_N + 1][d + 1];
		x = new double[item_N + 1][d + 1];
		feature_theta = new double[feature_N + 1][d + 1];
		feature_x = new double[feature_N + 1][d + 1];
		for (int i = 1; i <= user_N; ++i)
			for (int j = 1; j <= d; ++j)
				theta[i][j] = Math.random();
		for (int i = 1; i <= item_N; ++i)
			for (int j = 1; j <= d; ++j)
				x[i][j] = Math.random();
		for (int i = 1; i <= feature_N; ++i)
			for (int j = 1; j <= d; ++j) {
				feature_theta[i][j] = Math.random();
				feature_x[i][j] = Math.random();
			}
		rand = new Random();
	}

	public void Build_Mapping_Back() {
		userId_back_mapping = new HashMap<Integer, Long>();
		Set<Long> tmp = userId_mapping.keySet();
		Iterator<Long> iter = tmp.iterator();
		Long now;
		do {
			now = iter.next();
			userId_back_mapping.put(userId_mapping.get(now), now);
		} while (iter.hasNext());

		itemId_back_mapping = new HashMap<Integer, Long>();
		tmp = itemId_mapping.keySet();
		iter = tmp.iterator();
		do {
			now = iter.next();
			itemId_back_mapping.put(itemId_mapping.get(now), now);
		} while (iter.hasNext());
	}

	public void Build_user_To_item() {
		mp_user_item = new HashMap<Long, HashSet<Long>>();
		for (int i = 0; i < nodes.length; ++i) {
			HashSet<Long> item;
			if (mp_user_item.containsKey(nodes[i].getUserId()))
				item = mp_user_item.get(nodes[i].getUserId());
			else {
				item = new HashSet<>();
				mp_user_item.put(nodes[i].getUserId(), item);
			}
			item.add(nodes[i].getItemId());
		}
	}

	public void Build_id_To_feature() {
		mp_id_feature = new HashMap<Long, HashSet<Integer>>();
		HashSet<Integer> nowfeather;
		for (int i = 0; i < features.length; ++i) {
			if (mp_id_feature.containsKey(features[i].getId()))
				nowfeather = mp_id_feature.get(features[i].getId());
			else {
				nowfeather = new HashSet<Integer>();
				mp_id_feature.put(features[i].getId(), nowfeather);
			}
			nowfeather.addAll(features[i].getFeathers());
		}
		for (int i = 1; i <= user_N; ++i)
			if (mp_id_feature.get(userId_back_mapping.get(i)) == null) {
				nowfeather = new HashSet<Integer>();
				mp_id_feature.put(userId_back_mapping.get(i), nowfeather);
			}
		for (int i = 1; i <= item_N; ++i)
			if (mp_id_feature.get(itemId_back_mapping.get(i)) == null) {
				nowfeather = new HashSet<Integer>();
				mp_id_feature.put(itemId_back_mapping.get(i), nowfeather);
			}
	}
	
	public void Update(int p, int q, long op, long oq, double value) {
		double[] tmp1 = new double[d + 1];
		double[] tmp2 = new double[d + 1];
		for (int k = 1; k <= d; ++k) {
			tmp1[k] = theta[p][k];
			tmp2[k] = x[q][k];
		}
		for (int j = 1; j <= feature_N; ++j) {
			if (mp_id_feature.get(op).contains(j))
				for (int k = 1; k <= d; ++k)
					tmp1[k] += feature_theta[j][k];
			if (mp_id_feature.get(oq).contains(j))
				for (int k = 1; k <= d; ++k)
					tmp2[k] += feature_x[j][k];
		}
		for (int k = 1; k <= d; ++k)
			theta[p][k] -= alpha
					* ((Dot_Product(tmp1, tmp2, d) - value) * tmp2[k] + lamda
							* tmp1[k]);
		for (int k = 1; k <= d; ++k)
			x[q][k] -= alpha
					* ((Dot_Product(tmp1, tmp2, d) - value) * tmp1[k] + lamda
							* tmp2[k]);
		for (int j = 1; j <= feature_N; ++j) {
			if (mp_id_feature.get(op).contains(j))
				for (int k = 1; k <= d; ++k) {
					feature_theta[j][k] -= alpha
							* ((Dot_Product(tmp1, tmp2, d) - value)
									* tmp2[k] + lamda * tmp1[k]);
				}
			if (mp_id_feature.get(oq).contains(j))
				for (int k = 1; k <= d; ++k) {
					feature_x[j][k] -= alpha
							* ((Dot_Product(tmp1, tmp2, d) - value)
									* tmp1[k] + lamda * tmp2[k]);
				}
		}
	}

	/*
	 * 鎵ц姊害涓嬮檷绠楁硶
	 */
	public void gradientDescent() {
		Pre_Work();
		Build_Mapping_Back();
		Build_user_To_item();
		Build_id_To_feature();
		int round = 0, p, q;
		long op, oq;
		while (round <= max_round) {
			for (int i = 0; i < nodes.length; ++i) {
				p = (int) userId_mapping.get(nodes[i].getUserId());
				q = (int) itemId_mapping.get(nodes[i].getItemId());
				op = nodes[i].getUserId();
				oq = nodes[i].getItemId();
				Update(p, q, op, oq, 1.0);
				// 闅忔満璐熶緥
				for (int l = 1; l <= negative_example; ++l) {
					do {
						p = rand.nextInt(user_N) + 1;
					} while (userId_back_mapping.get(p) == null
							|| mp_user_item.get(userId_back_mapping.get(p)) == null);
					do {
						q = rand.nextInt(item_N) + 1;
					} while (itemId_back_mapping.get(q) == null
							|| mp_user_item.get(userId_back_mapping.get(p))
									.contains(itemId_back_mapping.get(q)));
					op = userId_back_mapping.get(p);
					oq = itemId_back_mapping.get(q);
					Update(p, q, op, oq, 0.0);
				}
				//System.err.println("round " + round + " node " + i);
			}
			double now = Cost_Function_with_Regulization();
			System.err.println(now + " alpha:" + alpha + " D:" + d + " lambda:"
					+ lamda + " round:" + round);
			// double delta = Math.abs(now - prev);
			// if (now >= prev) Configuration.alpha /= 2.0; else
			// Configuration.alpha *= 1.5;
			// prev = now;
			++round;
		}
	}

	/*
	 * 杩斿洖id涓篿鐨剈ser瀵筰d涓簀鐨刬tem鐨勮瘎鍒�
	 */
	public double get(long X, long Y) {
		double res;
		double[] tmp1 = new double[d + 1];
		double[] tmp2 = new double[d + 1];
		for (int k = 1; k <= d; ++k) {
			tmp1[k] = theta[userId_mapping.get(X)][k]; 
			tmp2[k] = x[itemId_mapping.get(Y)][k];
		}
		for (int j = 1; j <= feature_N; ++j) {
			if (mp_id_feature.get(X).contains(j))
				for (int k = 1; k <= d; ++k)
					tmp1[k] += feature_theta[j][k];
			if (mp_id_feature.get(Y).contains(j))
				for (int k = 1; k <= d; ++k)
					tmp2[k] += feature_x[j][k];
		}
		res = Dot_Product(tmp1, tmp2, d);
		return res;
	}

	/*
	 * 缁欏畾鏁版嵁鍜屽弬鏁帮紝濮戜笖浣跨敤鐨刲amda閮戒竴鏍�
	 */
	public GradientDescent(DataNode[] nodes, Feature[] features, int d,
			int negative_example, double lamda,
			HashMap<Long, Integer> userId_mapping,
			HashMap<Long, Integer> itemId_mapping) {
		this.lamda = lamda;
		this.d = d;
		this.nodes = nodes;
		this.features = features;
		this.userId_mapping = userId_mapping;
		this.itemId_mapping = itemId_mapping;
		this.negative_example = negative_example;
	}

}