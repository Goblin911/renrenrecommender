package tools;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.renren.api.json.JSONObject;
import com.renren.api.json.JSONTokener;

public class Tools {
	public static void show(Object t) {
		Gson g = new Gson();
		JsonElement json = g.toJsonTree(t);
		String res = g.toJson(json);
		JSONTokener tokener = new JSONTokener(res);
		try {
			JSONObject finalResult = new JSONObject(tokener);
			System.out.println(finalResult.toString(4));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String toString(Object t) {
		Gson g = new Gson();
		JsonElement json = g.toJsonTree(t);
		String res = g.toJson(json);
		return res;
	}
}
